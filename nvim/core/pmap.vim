"--------------------------------------------------------------------------------------------------"
"--------------------------------------appearance--------------------------------------------------"
"--------------------------------------------------------------------------------------------------"
"--------------------------"
"     dashboard-nvim       "
"--------------------------"
nmap <Leader>bs :<C-u>SessionSave<CR>
nmap <Leader>bl :<C-u>SessionLoad<CR>
nmap <Leader>bnn :<C-u>DashboardNewFile<CR>
nnoremap <silent> <Leader>fh :<C-u>Clap history<CR>
nnoremap <silent> <Leader>ff :<C-u>Clap files ++finder=rg --ignore --hidden --files<cr>
nnoremap <silent> <Leader>bc :<C-u>Clap colors<CR>
nnoremap <silent> <Leader>ba :<C-u>Clap grep2<CR>
nnoremap <silent> <Leader>fb :<C-u>Clap marks<CR>
nnoremap <silent> <Leader>fn :<C-u>DashboardNewFile<CR>


let g:dashboard_custom_shortcut={
  \ 'last_session'       : 'SPC b l',
  \ 'find_history'       : 'SPC f h',
  \ 'find_file'          : 'SPC f f',
  \ 'new_file'           : 'SPC b n',
  \ 'change_colorscheme' : 'SPC b c',
  \ 'find_word'          : 'SPC f w',
  \ 'book_marks'         : 'SPC f b',
  \ }

"--------------------------------------------------------------------------------------------------"
"--------------------------------------edit--------------------------------------------------"
"--------------------------------------------------------------------------------------------------"
"--------------------------"
"        vim-mundo         "
"--------------------------"

nnoremap <silent> <Leader>u :MundoToggle<CR>



"--------------------------"
"        EasyMotion        "
"--------------------------"
nmap <leader>j <Plug>(easymotion-w)
nmap <leader>k <Plug>(easymotion-b)
nmap <leader>w <Plug>(easymotion-overwin-f)


"--------------------------"
"        caw.vim	    	"
"--------------------------"
function! InitCaw() abort
	if ! (&l:modifiable && &buftype ==# '')
		silent! nunmap <buffer> <leader>cc
		silent! xunmap <buffer> <leader>cc
		silent! nunmap <buffer> <leader>c
		silent! xunmap <buffer> <leader>c
	else
		nmap <buffer> <leader>cc <Plug>(caw:prefix)
		xmap <buffer> <leader>cc <Plug>(caw:prefix)
		nmap <buffer> <leader>c <Plug>(caw:hatpos:toggle)
		xmap <buffer> <leader>c <Plug>(caw:hatpos:toggle)
	endif
endfunction
autocmd FileType * call InitCaw()
call InitCaw()


"--------------------------"
"        vim-sandwich   	"
"--------------------------"
nmap <silent> sa <Plug>(operator-sandwich-add)
xmap <silent> sa <Plug>(operator-sandwich-add)
omap <silent> sa <Plug>(operator-sandwich-g@)
nmap <silent> sd <Plug>(operator-sandwich-delete)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-query-a)
xmap <silent> sd <Plug>(operator-sandwich-delete)
nmap <silent> sr <Plug>(operator-sandwich-replace)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-query-a)  xmap <silent> sr <Plug>(operator-sandwich-replace)
nmap <silent> sdb <Plug>(operator-sandwich-delete)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-auto-a)
nmap <silent> srb <Plug>(operator-sandwich-replace)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-auto-a)
omap ib <Plug>(textobj-sandwich-auto-i)
xmap ib <Plug>(textobj-sandwich-auto-i)
omap ab <Plug>(textobj-sandwich-auto-a)
xmap ab <Plug>(textobj-sandwich-auto-a)
omap is <Plug>(textobj-sandwich-query-i)
xmap is <Plug>(textobj-sandwich-query-i)
omap as <Plug>(textobj-sandwich-query-a)
xmap as <Plug>(textobj-sandwich-query-a)


"--------------------------"
"       vim-operator-replace "
"--------------------------"
xmap p <Plug>(operator-replace)


"--------------------------"
"       vim-expand-region "
"--------------------------"
xmap + <Plug>(expand_region_expand)
xmap - <Plug>(expand_region_shrink)
vmap + <Plug>(expand_region_expand)
vmap - <Plug>(expand_region_shrink)


"--------------------------"
"       splitjoin.vim      "
"--------------------------"
let g:splitjoin_join_mapping = ''
let g:splitjoin_split_mapping = ''
nmap sj :SplitjoinJoin<CR>
nmap sk :SplitjoinSplit<CR>


"--------------------------"
"       dsf.vim            "
"--------------------------"
nmap dsf <Plug>DsfDelete
nmap csf <Plug>DsfChange

"--------------------------"
"      vim-niceblock       "
"--------------------------"
silent! xmap I  <Plug>(niceblock-I)
silent! xmap gI <Plug>(niceblock-gI)
silent! xmap A  <Plug>(niceblock-A)



"--------------------------"
"vim-textobj-multiblock    "
"--------------------------"
omap <silent> ab <Plug>(textobj-multiblock-a)
omap <silent> ib <Plug>(textobj-multiblock-i)
xmap <silent> ab <Plug>(textobj-multiblock-a)
xmap <silent> ib <Plug>(textobj-multiblock-i)



"--------------------------"
"kana/vim-textobj-function "
"--------------------------"

omap <silent> af <Plug>(textobj-function-a)
omap <silent> if <Plug>(textobj-function-i)
xmap <silent> af <Plug>(textobj-function-a)
xmap <silent> if <Plug>(textobj-function-i)


"--------------------------------------------------------------------------------------------------"
"--------------------------------------completion--------------------------------------------------"
"--------------------------------------------------------------------------------------------------"
"--------------------------"
"     Coc Keymap           "
"--------------------------"
" Remap for do codeAction of selected region
function! s:cocActionsOpenFromSelected(type) abort
    execute 'CocCommand actions.open ' . a:type
endfunction
xmap <silent> <Leader>a :<C-u>execute 'CocCommand actions.open ' . visualmode()<CR>
nmap <silent> <Leader>a :<C-u>set operatorfunc=<SID>cocActionsOpenFromSelected<CR>g@
" Do default action for next item.
nmap <silent> [a  :<C-u>CocNext<CR>
" Do default action for previous item.
nmap <silent> ]a  :<C-u>CocPrev<CR>
" Use [e and ]e for navigate diagnostics
nmap <silent> ]e <Plug>(coc-diagnostic-prev)
nmap <silent> [e <Plug>(coc-diagnostic-next)
" Remap for rename current word
nmap <Leader>cn <Plug>(coc-rename)
" Remap for format selected region
vmap <Leader>cf  <Plug>(coc-format-selected)
nmap <Leader>cf  <Plug>(coc-format-selected)
" Fix autofix problem of current line
nmap <Leader>cF  <Plug>(coc-fix-current)
" Remap keys for gotos
nmap <silent> gd :<C-u>call initself#definition_other_window()<CR>
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> <Leader>ci <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Use K for show documentation in float window
nnoremap <silent> K :call CocActionAsync('doHover')<CR>
" use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
nmap ]g <Plug>(coc-git-prevchunk)
nmap [g <Plug>(coc-git-nextchunk)
" show chunk diff at current position
nmap <Leader>gi <Plug>(coc-git-chunkinfo)
" show commit contains current position
nmap <Leader>gm <Plug>(coc-git-commit)
" float window scroll
nnoremap <expr><C-f> coc#util#has_float() ? coc#util#float_scroll(1) : "\<C-f>"
nnoremap <expr><C-b> coc#util#has_float() ? coc#util#float_scroll(0) : "\<C-b>"
" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
" Add :OR command for organize imports of the current buffer.
command! -nargs=0 OR  :call CocAction('runCommand', 'editor.action.organizeImport')
nnoremap <silent> <Leader>co :<C-u>OR<CR>
" multiple cursors
nmap <silent><M-s> <Plug>(coc-cursors-position)
nmap <expr> <silent><M-d> initself#select_current_word()
xmap <silent><M-d> <Plug>(coc-cursors-range)
nmap <silent><M-c>  <Plug>(coc-cursors-operator)

" Use :Format for format current buffer
command! -nargs=0 Format :call CocAction('format')

nnoremap  <Leader>fz :<C-u>CocSearch -w<Space>
" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)
nmap gcj :execute 'CocCommand docthis.documentThis'<CR>










"--------------------------------------------------------------------------------------------------"
"--------------------------------------navigate--------------------------------------------------"
"--------------------------------------------------------------------------------------------------"
"--------------------------"
"        defx.nvim'        "
"--------------------------"

nnoremap <silent> <Leader>e
  \ :<C-u>Defx -resume -toggle -buffer-name=tab`tabpagenr()`<CR>
nnoremap <silent> <Leader>F
  \ :<C-u>Defx -resume -buffer-name=tabtabpagenr()` -search=`expand('%:p')`<CR>
nnoremap <silent> <Leader>od :DBUIToggle<CR>
nnoremap <silent> <Leader>e
  \ :<C-u>Defx -resume -toggle -buffer-name=tab`tabpagenr()`<CR>
nnoremap <silent> <Leader>F
  \ :<C-u>Defx -resume -buffer-name=tabtabpagenr()` -search=`expand('%:p')`<CR>


"--------------------------"
"        vim-buffet        "
"--------------------------"

nnoremap bp :<C-u>bp<CR>
nnoremap bn :<C-u>bn<CR>
nnoremap <silent> <Leader>bc :Bonly<CR>          "<silent> 表示静默映射，不会显示Vim在处理rhs过程中对界面产生的变化。
nnoremap <silent> <Leader>bx :Bw<CR>			"关闭buffer
nmap <leader>1 <Plug>BuffetSwitch(1)
nmap <leader>2 <Plug>BuffetSwitch(2)
nmap <leader>3 <Plug>BuffetSwitch(3)
nmap <leader>4 <Plug>BuffetSwitch(4)
nmap <leader>5 <Plug>BuffetSwitch(5)
nmap <leader>6 <Plug>BuffetSwitch(6)
nmap <leader>7 <Plug>BuffetSwitch(7)
nmap <leader>8 <Plug>BuffetSwitch(8)
nmap <leader>9 <Plug>BuffetSwitch(9)
nmap <leader>0 <Plug>BuffetSwitch(10)








"--------------------------"
"     vim-clap Keymap      "
"--------------------------"
nnoremap <silent> <Leader>tc :<C-u>Clap colors<CR>
nnoremap <silent> <Leader>bb :<C-u>Clap buffers<CR>
nnoremap <silent> <Leader>fa :<C-u>Clap grep2<CR>
nnoremap <silent> <Leader>fb :<C-u>Clap marks<CR>
"like emacs counsel-find-file
nnoremap <silent> <C-x><C-f> :<C-u>Clap filer<CR>
nnoremap <silent> <Leader>ff :<C-u>Clap files ++finder=rg --ignore --hidden --files<cr>
nnoremap <silent> <Leader>fg :<C-u>Clap gfiles<CR>
nnoremap <silent> <Leader>fw :<C-u>Clap grep ++query=<cword><cr>
nnoremap <silent> <Leader>fh :<C-u>Clap history<CR>
nnoremap <silent> <Leader>fW :<C-u>Clap windows<CR>
nnoremap <silent> <Leader>fl :<C-u>Clap loclist<CR>
nnoremap <silent> <Leader>fu :<C-u>Clap git_diff_files<CR>
nnoremap <silent> <Leader>fv :<C-u>Clap grep ++query=@visual<CR>
nnoremap <silent> <Leader>oc :<C-u>Clap personalconf<CR>

"--------------------------"
"     coc-clap Keymap      "
"--------------------------"
" Show all diagnostics
nnoremap <silent> <Leader>ce  :Clap coc_diagnostics<CR>
" Manage extensions
nnoremap <silent> <Leader>;   :Clap coc_extensions<CR>
" Show commands
nnoremap <silent> <Leader>,   :Clap coc_commands<CR>
" Search workspace symbols
nnoremap <silent> <Leader>cs  :Clap coc_symbols<CR>
nnoremap <silent> <Leader>cS  :Clap coc_services<CR>
nnoremap <silent> <leader>ct  :Clap coc_outline<CR>



"--------------------------"
"     vista Keymap      "
"--------------------------"
nnoremap <silent> <Leader>i :<C-u>Vista!!<CR>




"--------------------------------------------------------------------------------------------------"
"--------------------------------------tool--------------------------------------------------"
"--------------------------------------------------------------------------------------------------"
"--------------------------"
"  accelerated_jk Keymap   "
"--------------------------"

nmap <silent>j <Plug>(accelerated_jk_gj)
nmap <silent>k <Plug>(accelerated_jk_gk)

"--------------------------"
"		floaterm Keymap	   "
"--------------------------"
nnoremap <silent> <Leader>tt :FloatermNew<CR>
tnoremap <silent> <Leader>tt <C-\><C-n>:FloatermNew<CR>
nnoremap <silent> <leader>tk :FloatermKill<CR>
tnoremap <silent> <leader>tk <C-\><C-n>:FloatermKill<CR>
nnoremap <silent> <Leader>tp :FloatermPrev<CR>
tnoremap <silent> <Leader>tp <C-\><C-n>:FloatermPrev<CR>
nnoremap <silent> <Leader>tn :FloatermNext<CR>
tnoremap <silent> <Leader>tn <C-\><C-n>:FloatermNext<CR>
nnoremap <silent> <Leader>t :FloatermToggle<CR>
tnoremap <silent> <Leader>t <C-\><C-n>:FloatermToggle<CR>



"--------------------------"
"		quickrun Keymap	   "
"--------------------------"
nnoremap <silent> <Leader>cr :QuickRun<CR>



















